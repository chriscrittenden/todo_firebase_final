import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import * as serviceWorker from './serviceWorker';
import {BrowserRouter, Route} from 'react-router-dom';
import {App, SignIn, SignUp} from './App';

ReactDOM.render((
    <BrowserRouter>
        <div>
            <Route exact path="/" component={SignIn}/>
            <Route exact path="/signup/" component={SignUp}/>
            <Route path="/app/" component={App}/>
        </div>
    </BrowserRouter>
    ), document.getElementById('root')
);

serviceWorker.unregister();
