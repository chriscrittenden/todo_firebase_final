import * as firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";

const config = {
    apiKey: "AIzaSyAvgeZ8VqLMTXTQt81F2BYdNllbCg5Af5s",
    authDomain: "fir-test-c2112.firebaseapp.com",
    databaseURL: "https://fir-test-c2112.firebaseio.com",
    projectId: "fir-test-c2112",
    storageBucket: "fir-test-c2112.appspot.com",
    messagingSenderId: "199347110137"
};

firebase.initializeApp(config);

export const auth = firebase.auth();

export const db = firebase.firestore();

export default firebase;