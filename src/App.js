import React, { useState, useEffect } from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import DeleteIcon from '@material-ui/icons/Delete';
import Drawer from '@material-ui/core/Drawer';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import ListIcon from '@material-ui/icons/List';
import AddIcon from '@material-ui/icons/Add';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import Checkbox from '@material-ui/core/Checkbox';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import {Route, Link} from 'react-router-dom';
import firebase, {auth, db} from './firebase';

export function SignIn(props) {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [error_message, setErrorMessage] = useState('');

    useEffect(() => {
        const unsubscribe = auth.onAuthStateChanged((user) => {
            if (user) {
                props.history.push('/app/')
            }
        });

        return unsubscribe;
    }, [props.history]);

    const handleSignIn = () => {
        auth.signInWithEmailAndPassword(email, password).catch((error) => {
            const error_message = error.message;
            setErrorMessage(error_message)
        });
    };

    return (
        <div style={{flexGrow: 1}}>
            <AppBar position='static'>
                <Toolbar>
                    <Typography variant='title' color='inherit' style={{flexGrow: 1}}>
                        Sign In
                    </Typography>
                </Toolbar>
            </AppBar>

            <div style={{display: 'flex', justifyContent: 'center', marginTop: '40px'}}>
                <Paper style={{width: '100%', maxWidth: '400px', padding: 40}}>
                    <TextField style={{marginBottom: 30}} placeholder='Email' fullWidth={true} value={email} onChange={(e) => {setEmail(e.target.value)}} />
                    <TextField type='password' style={{marginBottom: 30}} placeholder='Password' fullWidth={true} value={password} onChange={(e) => {setPassword(e.target.value)}} />
                    <div><Typography style={{color: 'red'}}>{error_message}</Typography></div>
                    <div style={{display: 'flex', justifyContent: 'space-between', width: '100%', alignItems: 'center'}}>
                        <div><Typography>Don't have an account? <Link to={'/signup/'}>Sign Up!</Link></Typography></div>
                        <Button color='primary' onClick={handleSignIn} variant={'raised'}>Sign In</Button>
                    </div>
                </Paper>
            </div>
        </div>
    );
}

export function SignUp(props) {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [error_message, setErrorMessage] = useState('')

    useEffect(() => {
        const unsubscribe = auth.onAuthStateChanged((user) => {
            if (user) {
                props.history.push('/app/')
            }
        });

        return unsubscribe;
    }, [props.history]);

    const handleSignUp = () => {
        auth.createUserWithEmailAndPassword(email, password).catch((error) => {
            const error_message = error.message;
            setErrorMessage(error_message)
        });
    };

    return (
        <div style={{flexGrow: 1}}>
            <AppBar position='static'>
                <Toolbar>
                    <Typography variant='title' color='inherit' style={{flexGrow: 1}}>
                        Sign Up
                    </Typography>
                </Toolbar>
            </AppBar>

            <div style={{display: 'flex', justifyContent: 'center', marginTop: '40px'}}>
                <Paper style={{width: '100%', maxWidth: '400px', padding: 40}}>
                    <TextField style={{marginBottom: 30}} placeholder='Email' fullWidth={true} value={email} onChange={(e) => {setEmail(e.target.value)}} />
                    <TextField type='password' style={{marginBottom: 30}} placeholder='Password' fullWidth={true} value={password} onChange={(e) => {setPassword(e.target.value)}} />
                    <div><Typography style={{color: 'red'}}>{error_message}</Typography></div>
                    <div style={{display: 'flex', justifyContent: 'space-between', width: '100%', alignItems: 'center'}}>
                        <div><Typography>Already have an account? <Link to={'/'}>Sign In!</Link></Typography></div>
                        <Button color='primary' onClick={handleSignUp} variant={'raised'}>Sign Up</Button>
                    </div>
                </Paper>
            </div>
        </div>
    );
}

export function App(props) {
    const [drawer_open, setDrawerOpen] = useState(false);
    const [add_list_open, setAddListOpen] = useState(false);
    const [lists, setLists] = useState([]);
    const [new_list, setNewList] = useState('');
    const [user, setUser] = useState(null);

    useEffect(() => {
        const unsubscribe = auth.onAuthStateChanged((user) => {
            if (user) {
                setUser(user);
            } else {
                props.history.push('/');
            }
        });

        return unsubscribe;
    }, [user, props.history]);

    useEffect(() => {
        let unsubscribe;

        if (user) {
            unsubscribe = db.collection('users').doc(user.uid).collection('lists').onSnapshot((snapshot) => {
                const updated_lists = [];
                snapshot.forEach((doc) => {
                    const data = doc.data();
                    updated_lists.push({...data, id: doc.id})
                });
                setLists(updated_lists);
            });
        }

        return unsubscribe
    }, [props.history, user]);

    const handleAddList = () => {
        if (new_list !== '') {
            db.collection('users').doc(user.uid).collection('lists').add({'name': new_list}).then(() => {
                setNewList('');
                setAddListOpen(false);
            })
        }
    };

    const handleSignOut = () => {
        firebase.auth().signOut()
    };

    if (!user) {
        return (<div/>)
    }

    return (
        <div style={{flexGrow: 1}}>
            {/* The AppBar */}
            <AppBar position='static'>
                <Toolbar>
                    <IconButton style={{marginLeft: -12, marginRight: 20}} color='inherit' onClick={() => {setDrawerOpen(true)}}>
                        <MenuIcon />
                    </IconButton>
                    <Route path={'/app/list/:list_id/'} render={(routeProps) => {
                        return (
                            <Typography variant='title' color='inherit' style={{flexGrow: 1}}>{lists.length > 0 && lists.find(list => list.id === routeProps.match.params.list_id).name}</Typography>
                        )}}
                    />
                    <Route exact path={'/app/'} render={(routeProps) => {
                        return (
                            <Typography variant='title' color='inherit' style={{flexGrow: 1}}>To Do List</Typography>
                        )
                    }}/>
                    <Button color='inherit' onClick={handleSignOut}>Sign Out</Button>
                </Toolbar>
            </AppBar>

            {/* The Drawer */}
            <Drawer open={drawer_open} onClose={() => {setDrawerOpen(false)}}>
                <div>
                    {lists.map(list => {
                        return (
                            <ListItem key={list.id} button onClick={() => {setDrawerOpen(false)}} component={Link} to={`/app/list/${list.id}/`}>
                                <ListItemIcon>
                                    <ListIcon />
                                </ListItemIcon>
                                <ListItemText primary={list.name} />
                            </ListItem>
                        )
                    })}
                    <ListItem button onClick={() => {setAddListOpen(true)}}>
                        <ListItemIcon>
                            <AddIcon />
                        </ListItemIcon>
                        <ListItemText primary='Create List' />
                    </ListItem>
                </div>
            </Drawer>

            {/* The List */}
            <Route path={'/app/list/:list_id/'} render={routeProps => {
                const list_id = routeProps.match.params.list_id;
                const list = lists.find(list => list.id === list_id);

                return (
                    <List {...routeProps} list={list} uid={user.uid}/>
                )
            }}/>

            {/*The Add List Dialog*/}
            <Dialog open={add_list_open} onClose={() => {setAddListOpen(false)}}>
                <DialogTitle>Create a new list</DialogTitle>
                <DialogContent>
                    <ListItem>
                        <ListItemText primary={<TextField placeholder='Name your list' style={{width: '100%'}} value={new_list} onChange={(e) => {setNewList(e.target.value)}} />} />
                        <ListItemIcon>
                            <IconButton onClick={handleAddList}><AddIcon/></IconButton>
                        </ListItemIcon>
                    </ListItem>
                </DialogContent>
            </Dialog>
        </div>
    );
}

function List(props) {
    const [new_task, setNewTask] = useState('');
    const [tasks, setTasks] = useState([]);

    const list_id = props.match.params.list_id;
    const list_ref = db.collection('users').doc(props.uid).collection('lists').doc(list_id);
    const tasks_ref = list_ref.collection('tasks');

    useEffect(() => {
        const unsubscribe = db.collection('users').doc(props.uid).collection('lists').doc(list_id).collection('tasks').onSnapshot((snapshot) => {
            const updated_tasks = [];
            snapshot.forEach((doc) => {
                const data = doc.data();
                updated_tasks.push({...data, id: doc.id});
            });
            setTasks(updated_tasks);
        });

        return unsubscribe;
    }, [list_id, props.uid]);

    const handleAddTask = () => {
        if (new_task !== '') {
            tasks_ref.add({'text': new_task, 'complete': false}).then(() => {
                setNewTask('')
            })
        }
    };

    const handleCheck = (task_id, checked) => {
        tasks_ref.doc(task_id).update({complete: checked});
    };

    const handleDeleteTask = (task_id) => {
        tasks_ref.doc(task_id).delete();
    };

    const handleDeleteList = () => {
        props.history.push('/');
        list_ref.delete();
    };

    if (!props.list) {
        return (<div/>)
    }

    return (
        <div style={{display: 'flex', justifyContent: 'center'}}>
            <Paper style={{maxWidth: '1020px', width: '100%', marginTop: '40px'}}>
                <div>
                    <div style={{display: 'flex', padding: '24px 24px'}}>
                        <Typography variant='headline' style={{flexGrow: 1}}>{props.list.name}</Typography>
                        <Button color='secondary' variant='raised' size='small' onClick={handleDeleteList}>Delete List</Button>
                    </div>
                    <ListItem>
                        <ListItemText primary={<TextField placeholder='Type a new task' style={{width: '100%'}} value={new_task} onChange={(e) => {setNewTask(e.target.value)}}/>} />
                        <ListItemIcon><IconButton onClick={handleAddTask}><AddIcon/></IconButton></ListItemIcon>
                    </ListItem>
                    {tasks.map(task => {
                        return (
                            <ListItem key={task.id}>
                                <ListItemIcon>
                                    <Checkbox
                                        checked={task.complete}
                                        onChange={(e, checked) => {handleCheck(task.id, checked)}}
                                    />
                                </ListItemIcon>
                                <ListItemText primary={task.text}/>
                                <IconButton onClick={() => {handleDeleteTask(task.id)}}><DeleteIcon/></IconButton>
                            </ListItem>
                        )
                    })}
                </div>
            </Paper>
        </div>
    )
}